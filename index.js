const { ApolloServer } = require("apollo-server");

const typeDefs = require("./graphql/schema");
const resolvers = require("./graphql/resolver");
const mongoose = require("mongoose");

const server = new ApolloServer({
  typeDefs,
  resolvers,
});

mongoose.connect(
  "mongodb+srv://database-admin:HP67kWsq8OMDctrd@kronliu.hkvxe.mongodb.net/eCommerce?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

mongoose.connection.once("open", () =>
  console.log("Now connected to MongoDB Atlas.")
);

server.listen().then(({ url }) => {
  console.log(`server is running at ${url}`);
});
