const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const itemSchema = new Schema({
  name: String,
  description: String,
  unitPrice: Number,
  categoryId: Number,
});

module.exports = mongoose.model("item", itemSchema);
