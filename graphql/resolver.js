const User = require("../models/user");
const Item = require("../models/item");
const Category = require("../models/category");

module.exports = {
  Query: {
    getUsers() {
      return User.find({});
    },
    getItems() {
      return Item.find({});
    },
    getCategories() {
      return Category.find({});
    },
  },

  Mutation: {
    addUser(parent, args, ctx, info) {
      const newUser = new User({
        firstName: args.firstName,
        lastName: args.lastName,
        email: args.email,
      });

      newUser.save();
      return newUser;
    },
    addItem(parent, args, ctx, info) {
      const newItem = new Item({
        name: args.name,
        description: args.description,
        unitPrice: args.unitPrice,
        categoryId: args.categoryId,
      });

      newItem.save();
      return newItem;
    },
    addCategory(parent, args, ctx, info) {
      const newCategory = new Category({
        name: args.name,
        description: args.description,
      });

      newCategory.save();
      return newCategory;
    },
  },
};
