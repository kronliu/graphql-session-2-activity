const { gql } = require("apollo-server");

module.exports = gql`
  type Query {
    getUsers: [User]
    getItems: [Item]
    getCategories: [Category]
  }
  type User {
    id: ID!
    firstName: String
    lastName: String
    email: String
  }
  type Item {
    id: ID!
    name: String
    description: String
    unitPrice: Int
    categoryId: Int
  }
  type Category {
    id: ID!
    name: String
    description: String
  }

  type Mutation {
    addUser(firstName: String, lastName: String, email: String): User
    addItem(
      name: String
      description: String
      unitPrice: Int
      categoryId: Int
    ): Item
    addCategory(name: String, description: String): Category
  }
`;
